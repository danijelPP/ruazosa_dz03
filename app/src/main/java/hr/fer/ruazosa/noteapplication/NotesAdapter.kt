package hr.fer.ruazosa.noteapplication

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView

class NotesAdapter(
    listOfNotesViewModel: NotesViewModel,
    onNoteListener: OnNoteListener,
    onRemoveNoteListener: OnRemoveNoteListener
) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    var listOfNotes: NotesViewModel = listOfNotesViewModel
    var onNoteListener: OnNoteListener = onNoteListener
    var onRemoveNoteListener: OnRemoveNoteListener = onRemoveNoteListener

    class ViewHolder(
        itemView: View,
        onNoteListener: OnNoteListener,
        onRemoveNoteListener: OnRemoveNoteListener
    ) :
        RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var noteTitleTextView: TextView? = null
        var noteDateTextView: TextView? = null
        var removeButtonView: ImageButton? = null
        var onNoteListener: OnNoteListener? = null
        var onRemoveNoteListener: OnRemoveNoteListener? = null

        init {
            noteTitleTextView = itemView.findViewById(R.id.noteTitleTextView)
            noteDateTextView = itemView.findViewById(R.id.noteDateTextView)
            removeButtonView = itemView.findViewById(R.id.removeButton)
            removeButtonView?.setOnClickListener(this)
            this.onNoteListener = onNoteListener
            this.onRemoveNoteListener = onRemoveNoteListener
            this.noteTitleTextView?.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            if (p0 != null) {
                if (p0.equals(removeButtonView))
                    onRemoveNoteListener?.onRemoveNoteClick(adapterPosition)
                else onNoteListener?.onNoteClick(adapterPosition)
            } else onNoteListener?.onNoteClick(adapterPosition)
        }
    }

    interface OnNoteListener {
        fun onNoteClick(position: Int)
    }

    interface OnRemoveNoteListener {
        fun onRemoveNoteClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): NotesAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val notesListElement = inflater.inflate(R.layout.note_list_element, parent, false)
        return ViewHolder(notesListElement, onNoteListener,onRemoveNoteListener)
    }

    override fun getItemCount(): Int {
        if (listOfNotes.notesList.value != null) {
            return listOfNotes.notesList.value!!.count()
        }
        return 0
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (listOfNotes != null) {
            viewHolder.noteTitleTextView?.text = listOfNotes.notesList.value!![position].noteTitle


            viewHolder.noteDateTextView?.text =
                listOfNotes.notesList.value!![position].noteDate.toString()
        }
    }
}