package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.util.Log
import kotlinx.android.synthetic.main.activity_notes_details.*
import java.util.*

class NotesDetails : AppCompatActivity() {
    var oldNotePosition: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_details)
        Log.d("loc", "called create")
        val viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java
            )
        val extras = intent.extras
        if (extras != null) {
            var position: Int = extras["position"] as Int;
            oldNotePosition = position
            Log.d("loc", position.toString())
            var note: Note = viewModel.getNoteFromRepository(position)
            var title: String? = note.noteTitle
            var desc: String? = note.noteDescription
            noteTitleEditText.setText(title)
            noteDescriptionEditText.setText(desc)
        } else Log.d("loc", "empty extras")




        saveNoteButton.setOnClickListener {
            var note = Note()
            note.noteTitle = noteTitleEditText.text.toString()
            note.noteDescription = noteDescriptionEditText.text.toString()
            note.noteDate = Date()
            if (oldNotePosition != null) {
                viewModel.updateNoteInRepository(oldNotePosition!!, note)
            } else viewModel.saveNoteToRepository(note)
            finish()
        }
    }


}
