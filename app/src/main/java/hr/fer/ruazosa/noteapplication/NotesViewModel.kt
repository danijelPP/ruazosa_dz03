package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import java.lang.IndexOutOfBoundsException

class NotesViewModel : ViewModel() {
    var notesList = MutableLiveData<List<Note>>()

    fun getNotesRepository() {
        notesList.value = NotesRepository.notesList
    }

    fun saveNoteToRepository(note: Note) {
        NotesRepository.notesList.add(note)
    }

    fun updateNoteInRepository(position: Int, note: Note) {
        NotesRepository.notesList[position] = note
    }

    fun removeNoteFromRepository(position: Int) {
        if (position < NotesRepository.notesList.size)
            NotesRepository.notesList.removeAt(position)
    }

    fun getNoteFromRepository(position: Int): Note {
        if (position < NotesRepository.notesList.size)
            return NotesRepository.notesList[position]
        else throw IndexOutOfBoundsException()
    }
}